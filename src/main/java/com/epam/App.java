package com.epam;

import com.epam.controller.Controller;
import com.epam.controller.ControllerImpl;

public class App {

    public static void main(String[] args) {
        Controller controller = new ControllerImpl();
        controller.start();
    }

}
