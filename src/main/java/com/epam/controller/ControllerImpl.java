package com.epam.controller;

import com.epam.model.Text;
import com.epam.view.Menu;
import com.epam.view.Option;
import com.epam.view.View;
import com.epam.view.ViewImpl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class ControllerImpl implements Controller {

    private View view;

    public ControllerImpl() {
        view = new ViewImpl();
    }

    @Override
    public void start() {
        Text text = new Text();

        Menu textManipulations = new Menu(Integer.MAX_VALUE);
        textManipulations.addMenuOption("1", new Option("Print sentence with biggest word occurrence", text::printSentenceWithBiggestWordOccurrence));
        textManipulations.addMenuOption("2", new Option("Print sentences ordered by amount of words", text::printSentencesOrderedByAmountOfWords));
        textManipulations.addMenuOption("3", new Option("Print word in first sentence that doesn't occurs in others", text::printWordInFirstSentenceThatDoesntOccursInOthers));
        textManipulations.addMenuOption("4", new Option("Print all words with given size", () -> {
            int size = Integer.parseInt(view.userInput("Enter word size"));
            text.printAllWordsWithGivenSize(size);
        }));
        textManipulations.addMenuOption("5", new Option("Swap first vowel word with biggest word", text::swapFirstVowelWordWithBiggestWord));
        textManipulations.addMenuOption("6", new Option("Print words in alphabetic order", text::printWordsInAlphabeticOrder));
        textManipulations.addMenuOption("7", new Option("Print words ordered by vowel ratio", text::printWordsOrderedByVowelRatio));
        textManipulations.addMenuOption("9", new Option("Print words by entered letter", () -> {
            text.printWordsByEnteredLetter(view.userInput("Enter a letter: ").charAt(0), -1);
        }));
        textManipulations.addMenuOption("10", new Option("Print words occurrence in text", () -> {
            String input = view.userInput("Enter a set of words with delimiter(' ', ',', ';'): ");
            StringTokenizer stringTokenizer = new StringTokenizer(input, " ,;");
            List<String> words = new ArrayList<>();
            while (stringTokenizer.hasMoreTokens()) {
                words.add(stringTokenizer.nextToken());
            }
            text.printWordsOccurrenceInText(words);
        }));
        textManipulations.addMenuOption("11", new Option("Delete substring", () -> {
            char start = view.userInput("Enter first letter: ").charAt(0);
            char end = view.userInput("Enter second letter: ").charAt(0);
            text.deleteSubstring(start, end);
        }));
        textManipulations.addMenuOption("12", new Option("Delete all consonant words", () -> {
            text.deleteAllConsonantWords(Integer.parseInt(view.userInput("Enter word size: ")));
        }));
        textManipulations.addMenuOption("13", new Option("Print words by entered letter", () -> {
            text.printWordsByEnteredLetter(view.userInput("Enter a letter: ").charAt(0), 1);
        }));
        textManipulations.addMenuOption("15", new Option("Delete occurrence of first and last letter", text::deleteOccurrenceOfFirstAndLastLetter));
        textManipulations.addMenuOption("16", new Option("Replace the word with substring", () -> {
            while (true) {
                try {
                    text.replaceWordWithSubstring(
                            Integer.parseInt(view.userInput("Enter the sentence id: ")),
                            Integer.parseInt(view.userInput("Enter the word size: ")),
                            view.userInput("Enter the substring for replacement: "));
                    break;
                } catch (IndexOutOfBoundsException e) {}
            }
        }));
        textManipulations.addMenuOption("17", new Option("Print text", () -> System.out.println(text)));
        textManipulations.addMenuOption("Q", new Option("Exit", () -> System.exit(0)));

        Menu menu = new Menu(1);
        menu.addMenuOption("1", new Option("Enter the text ", () -> {
            text.setText(view.userInput("Enter a text: "));
        }));
        menu.addMenuOption("2", new Option("Load the text from file", () -> {
            File file;
            do {
                file = new File(view.userInput("Enter the file path: "));
            } while ( ! file.exists());
            StringBuilder fileText = new StringBuilder();
            try (BufferedReader br = new BufferedReader(new FileReader(file))) {
                while (br.ready()) {
                    fileText.append(br.readLine());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            text.setText(fileText.toString());
        }));
        menu.addMenuOption("N", new Option("Next", () -> {
            view.setMenu(textManipulations);
        }));

        view.setMenu(menu);
        view.showMenu();
    }



}
