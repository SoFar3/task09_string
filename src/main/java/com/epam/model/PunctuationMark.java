package com.epam.model;

enum PunctuationMark {

    PERIOD("."),
    COMMA(","),
    QUESTION_MARK("?"),
    EXCLAMATION_MARK("!");

    private String mark;

    PunctuationMark(String mark) {
        this.mark = mark;
    }

    static PunctuationMark getMark(String mark) {
        PunctuationMark[] values = values();
        for (PunctuationMark value : values) {
            if (value.mark.equals(mark)) {
                return value;
            }
        }
        throw new NullPointerException("There is no mark with sign " + mark);
    }

}
