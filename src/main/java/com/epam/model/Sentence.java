package com.epam.model;

import java.util.*;
import java.util.stream.Collectors;

public class Sentence {

    private String sentence;
    private List<Word> words;
    private Map<Word, Integer> wordOccurrence;
    private PunctuationMark lastMark;

    public Sentence() {
        this.words = new ArrayList<>();
        this.wordOccurrence = new HashMap<>();
    }

    public Sentence(String sentence) {
        this();
        this.sentence = sentence;
        parse();

        Word lastWord = words.get(words.size() - 1);
        lastMark = PunctuationMark.getMark(lastWord.getWord().charAt(lastWord.getWord().length() - 1) + "");

        wordOccurrence();
    }

    public List<Word> getWords() {
        return words;
    }

    public void addWord(Word word) {
        this.words.add(word);
        wordOccurrence();
    }

    public PunctuationMark getLastMark() {
        return lastMark;
    }

    public void setLastMark(PunctuationMark lastMark) {
        this.lastMark = lastMark;
    }

    public Map<Word, Integer> getWordOccurrence() {
        return wordOccurrence;
    }

    private void wordOccurrence() {
        for (Word word : words) {
            wordOccurrence.put(word,
                    wordOccurrence.get(word) == null
                            ? 1
                            : wordOccurrence.get(word) + 1);
        }
    }

    public int getMaxWordOccurrence() {
        Map.Entry<Word, Integer> max = wordOccurrence.entrySet().stream().max(Comparator.comparingInt(Map.Entry::getValue)).orElse(null);
        return max != null ? max.getValue() : 0;
    }

    public void swapFirstVowelWordWithBiggestWord() {
        int vowelWordIndex = 0;
        for (Word word : words) {
            if (word.getWord().toUpperCase().charAt(0) == 'A'
                    || word.getWord().toUpperCase().charAt(0) == 'E'
                    || word.getWord().toUpperCase().charAt(0) == 'I'
                    || word.getWord().toUpperCase().charAt(0) == 'O'
                    || word.getWord().toUpperCase().charAt(0) == 'U') {
                vowelWordIndex = words.indexOf(word);
                break;
            }
        }
        int indexLongestWord = words.indexOf(words.stream().max(Comparator.comparingInt(s0 -> s0.getWord().length())).orElse(null));
        Collections.swap(words, vowelWordIndex, indexLongestWord);
    }

    public void deleteNowVowelWords(int size) {
        List<Word> collect = words.stream()
                .filter(w -> w.getWord().length() == size)
                .filter(w -> w.getWord().toUpperCase().charAt(0) != 'A'
                        && w.getWord().toUpperCase().charAt(0) != 'E'
                        && w.getWord().toUpperCase().charAt(0) != 'I'
                        && w.getWord().toUpperCase().charAt(0) != 'O'
                        && w.getWord().toUpperCase().charAt(0) != 'U')
                .collect(Collectors.toList());
        words.removeAll(collect);
    }

    public void replaceWordWithSubstring(int size, String substring) {
        words.stream().filter(w -> w.getWord().length() == size).forEach(w -> words.set(words.indexOf(w), new Word(substring)));
    }

    public void deleteSubstring(Character start, Character end) {
        int startIdx = sentence.indexOf(start);
        int endIdx = sentence.lastIndexOf(end);
        System.out.println(sentence.substring(0, startIdx) + sentence.substring(endIdx + 1));
        this.sentence = sentence.substring(0, startIdx) + sentence.substring(endIdx + 1);
        rebuild();
    }

    private void parse() {
        StringTokenizer tokenizer = new StringTokenizer(sentence, " ,");
        while (tokenizer.hasMoreTokens()) {
            words.add(new Word(tokenizer.nextToken()));
        }
    }

    private void rebuild() {
        words.clear();
        parse();

        wordOccurrence.clear();
        wordOccurrence();
    }

    @Override
    public String toString() {
        StringBuilder sentence = new StringBuilder();
        for (Word word : words) {
            sentence.append(word.toString()).append(" ");
        }
        return sentence.toString();
    }

}
