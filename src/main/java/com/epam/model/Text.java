package com.epam.model;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.StringTokenizer;
import java.util.stream.Collectors;

public class Text {

    private String text;
    private List<Sentence> sentences;

    public Text() {
        this.sentences = new ArrayList<>();
    }

    public Text(String text) {
        this();
        this.text = text;
        parse();
    }

    public void addSentence(Sentence sentence) {
        sentences.add(sentence);
    }

    public List<Sentence> getSentences() {
        return sentences;
    }

    public void setSentences(List<Sentence> sentences) {
        this.sentences = sentences;
    }

    public void printSentenceWithBiggestWordOccurrence() {
        Sentence sentence = sentences.stream().max(Comparator.comparingInt(Sentence::getMaxWordOccurrence)).orElse(null);
        System.out.println(sentence);
    }

    public void printSentencesOrderedByAmountOfWords() {
        sentences.sort(Comparator.comparingInt(s0 -> s0.getWords().size()));
        System.out.println(sentences);
    }

    public void printWordInFirstSentenceThatDoesntOccursInOthers() {
        for (int i = 0; i < sentences.get(0).getWords().size(); i++) {
            boolean occur = false;
            for (int j = 1; j < sentences.size() - 1; j++) {
                for (int k = 0; k < sentences.get(j).getWords().size(); k++) {
                    if (sentences.get(0).getWords().get(i).equals(sentences.get(j).getWords().get(k))) {
                        occur = true;
                        break;
                    }
                }
            }
            if (! occur) {
                System.out.println(sentences.get(0).getWords().get(i));
            }
        }
    }

    public void printAllWordsWithGivenSize(int size) {
        sentences.stream()
                .filter(s -> s.getLastMark().equals(PunctuationMark.QUESTION_MARK))
                .map(Sentence::getWords)
                .forEach(
                        listOfWords -> listOfWords.stream()
                                .filter(w -> w.getWord().length() == size)
                                .forEach(w -> System.out.print(w + " ")));
    }

    public void swapFirstVowelWordWithBiggestWord() {
        for (Sentence sentence : sentences) {
            sentence.swapFirstVowelWordWithBiggestWord();
        }
    }

    public void printWordsInAlphabeticOrder() {
        StringBuilder space = new StringBuilder();
        List<Word> allWords = getAllWords();
        allWords.sort(Comparator.comparingInt(w0 -> w0.getWord().toLowerCase().charAt(0)));

        Character prev = allWords.get(0).getWord().charAt(0);
        for (Word allWord : allWords) {
            Character next = allWord.getWord().charAt(0);

            if (prev != next) {
                space.append(" ");
            }

            System.out.println(space.toString() + allWord.getWord());
            prev = next;
        }
    }

    public void printWordsOrderedByVowelRatio() {
        List<Word> allWords = getAllWords();
        allWords.sort(Comparator.comparingDouble(w -> -w.getVowelRatio()));
        allWords.forEach(w -> System.out.println(w + " " + w.getVowelRatio()));
    }

    public void printWordsByEnteredLetter(Character character, int order) {
        List<Word> allWords = getAllWords();
        List<Word> collect = allWords.stream()
                .filter(w -> w.getWord().indexOf(character) >= 0).sorted((w0, w1) -> {
                    int comparison = order * (w0.getLetterOccurrence(character) - w1.getLetterOccurrence(character));
                    if (comparison == 0) {
                        return order * w0.getWord().compareTo(w1.getWord());
                    }
                    return comparison;
                }).collect(Collectors.toList());
        collect.forEach(w -> System.out.println(w + " " + w.getLetterOccurrence(character)));
    }

    public void printWordsOccurrenceInText(List<String> words) {
        for (String word : words) {
            long count = sentences.stream()
                    .filter(s -> s.getWordOccurrence().get(new Word(word)) != null)
                    .mapToInt(s -> s.getWordOccurrence().get(new Word(word)))
                    .sum();
            System.out.println(word + " occurs in text: " + count);
        }
    }

    public void deleteSubstring(Character start, Character end) {
        sentences.forEach(s -> s.deleteSubstring(start, end));
    }

    public void deleteAllConsonantWords(int size) {
        sentences.forEach(s -> s.deleteNowVowelWords(size));
    }

    public void deleteOccurrenceOfFirstAndLastLetter() {
        sentences
                .forEach(s -> s.getWords()
                        .forEach(Word::deleteOccurrenceOfFirstAndLastLetter));
    }

    public void replaceWordWithSubstring(int sentenceId, int wordSize, String substring) {
        sentences.get(sentenceId).replaceWordWithSubstring(wordSize, substring);
    }

    public void setText(String text) {
        this.text = text;
        parse();
    }

    private void parse() {
        StringTokenizer tokenizer = new StringTokenizer(text, ".?!", true);
        while (tokenizer.hasMoreTokens()) {
            sentences.add(new Sentence(tokenizer.nextToken() + tokenizer.nextToken()));
        }
    }

    private List<Word> getAllWords() {
        List<Word> allWords = new ArrayList<>();
        for (Sentence sentence : sentences) {
            allWords.addAll(sentence.getWords());
        }
        return allWords;
    }

    @Override
    public String toString() {
        StringBuilder text = new StringBuilder();
        for (Sentence sentence : sentences) {
            text.append(sentence.toString()).append("\n");
        }
        return "Text {\n" + text.toString() + "}";
    }

}
