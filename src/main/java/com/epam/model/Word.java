package com.epam.model;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Word {

    private String word;
    private Map<Character, Integer> lettersOccurrence;

    public Word() {
        lettersOccurrence = new HashMap<>();
    }

    public Word(String word) {
        this();
        this.word = word;
        letterOccurrence();
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public double getVowelRatio() {
        long count = word.chars().filter(c -> c == 'A' || c == 'E' || c == 'I' || c == 'O' || c == 'U'
                || c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u').count();
        return 1.0 * count / word.length();
    }

    public int getLetterOccurrence(Character character) {
        return (int) word.chars().filter(c -> c == character).count();
    }

    //15
    public void deleteOccurrenceOfFirstAndLastLetter() {
        Character first = word.charAt(0);
        Character last = word.charAt(word.length() - 1);
        word = word.replaceAll(String.valueOf(first), "");
        word = word.replaceAll(String.valueOf(last), "");
    }

    private void letterOccurrence() {
        for (int i = 0; i < word.length(); i++) {
            lettersOccurrence.put(word.charAt(i),
                    lettersOccurrence.get(word.charAt(i)) == null
                            ? 1
                            : lettersOccurrence.get(word.charAt(i)) + 1);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Word word1 = (Word) o;
        return Objects.equals(word, word1.word);
    }

    @Override
    public int hashCode() {
        return Objects.hash(word);
    }

    @Override
    public String toString() {
        return word;
    }

}
